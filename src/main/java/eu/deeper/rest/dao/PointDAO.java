package eu.deeper.rest.dao;

import java.util.List;

import eu.deeper.rest.model.Point;

public interface PointDAO {

	public List<Point> findAll();
	
	public Point findById(long id);
	
	public void add(Point point);
	
	public void delete(long id);
	
	public void deleteAll(List<Point> points);
	
	public void delete(Point point);

}

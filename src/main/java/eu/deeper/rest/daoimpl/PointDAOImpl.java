package eu.deeper.rest.daoimpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eu.deeper.rest.dao.PointDAO;
import eu.deeper.rest.model.Point;

@Repository
@Transactional
public class PointDAOImpl implements PointDAO {
	
    @Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Point> findAll() {
		return sessionFactory.getCurrentSession().createQuery("from point").list();
	}

	@Override
	public void add(Point point) {
		sessionFactory.getCurrentSession().persist(point);
	}

	@Override
	public void delete(long id) {
		sessionFactory.getCurrentSession().delete(findById(id));
	}
	
	@Override
	public void delete(Point point) {
		sessionFactory.getCurrentSession().delete(point);
	}
	
	@Override
	public void deleteAll(List<Point> points) {
		for (Point point : points)
			sessionFactory.getCurrentSession().delete(point);
	}
	
	@Override
	public Point findById(long id){
		return (Point) sessionFactory.getCurrentSession().get(Point.class, id);
	}

}

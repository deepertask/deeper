package eu.deeper.rest.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="point")
@XmlRootElement
public class Point implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name = "x", nullable = false)
	private Double x;
	
	@Column(name = "y", nullable = false)
	private Double y;
	
	private String color;
	
	private String name;
	
	public Point(){}
	
	public Point(Double x, Double y){
		this.x = x;
		this.y = y;
	}
	
	public Point(Double x, Double y, String name, String color){
		this.x = x;
		this.y = y;
		this.name = name;
		this.color = color;
	}
	
	public Long getId() {
		return id;
	}

	@XmlElement
	public void setId(Long id) {
		this.id = id;
	}

	public Double getX() {
		return x;
	}

	@XmlElement
	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	@XmlElement
	public void setY(Double y) {
		this.y = y;
	}

	public String getColor() {
		return color;
	}

	@XmlElement
	public void setColor(String color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Point[X=" + x + "; Y=" + y + "; color=" + color + "; name=" + name + "; id=" + id + "]";
	}
	
}

package eu.deeper.rest.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "response")
public class Response {
	
	private int code;
	
	private String message;
	
	public Response(){}
	
	public Response (int code, String message){
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	@XmlElement
	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	@XmlElement
	public void setMessage(String message) {
		this.message = message;
	}

}

package eu.deeper.rest.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SortedPoints {
	
	double totalDistance;
	
	List<Point> point;
	
	public SortedPoints() {}
	
	public SortedPoints(double totalDistance, List<Point> points) {
		super();
		this.totalDistance = totalDistance;
		this.point = points;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	public List<Point> getPoint() {
		return point;
	}

	public void setPoint(List<Point> point) {
		this.point = point;
	}

}

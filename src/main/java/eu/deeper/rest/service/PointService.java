package eu.deeper.rest.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import eu.deeper.rest.dao.PointDAO;
import eu.deeper.rest.model.Point;
import eu.deeper.rest.model.Response;
import eu.deeper.rest.model.SortedPoints;

@Service
@Path("/points")
public class PointService {

	Logger logger = Logger.getLogger(PointService.class);

	@Autowired
	PointDAO pointDAO;

	@POST 
	@Path("/add")
	@Produces({MediaType.APPLICATION_XML})	
	public Response createPodcastFromForm(
			@QueryParam("x") String x,
			@QueryParam("y") String y,
			@DefaultValue("") @QueryParam("name") String name,
			@DefaultValue("") @QueryParam("color") String color
			) {

		if (isNumeric(x) && isNumeric(y)){

			Point point = new Point(Double.valueOf(x), Double.valueOf(y), name, color);
			logger.debug("Adding point " + point);

			pointDAO.add(point);

			return new Response(HttpStatus.CREATED.value(), point + " has been created");
		} else 
			return new Response(HttpStatus.BAD_REQUEST.value(), "Invalid point paremeters!");
	}


	@POST
	@Path("/addPoints")
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	public Response addPoints(Point[] points) {

		for (Point point : points)
			if (isValidPoint(point)){
				logger.debug("Adding point " + point);
				pointDAO.add(point);
			} else 
				logger.warn("Invalid point " + point);

		return new Response(HttpStatus.CREATED.value(), points.length + " points have been added");
	}

	@DELETE
	@Path("/deleteAll")
	@Produces(MediaType.APPLICATION_XML)
	public Response deleteAll() {

		logger.debug("Delete all points");

		List<Point> points = pointDAO.findAll();

		pointDAO.deleteAll(points);

		return new Response(HttpStatus.OK.value(), points.size() + " points have been removed!");
	}

	@DELETE
	@Path("/deleteById/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response deleteById(@PathParam("id") Long id) {

		logger.debug("Remove point by id: " + id);

		if (id != null) {

			Point point = pointDAO.findById(id);

			if (point != null){
				pointDAO.delete(point);
				return new Response(HttpStatus.OK.value(), point + " has been removed!");
			} else {
				return new Response(HttpStatus.NOT_MODIFIED.value(), "Point to delete with id " + id + " was not found in DB");
			}

		} else 
			return new Response(HttpStatus.BAD_REQUEST.value(), "Please provider id for point to delete!");
	}

	private double getDistance(Point point1, Point point2){
		return Math.sqrt(Math.pow(point1.getX()- point2.getX(), 2) + Math.pow(point1.getY() - point2.getY(), 2));
	}

	@GET
	@Path("/sorted")
	@Produces(MediaType.APPLICATION_XML)
	public SortedPoints getSortedPoints() {

		logger.debug("GetSortedPoints request..."); 
		
		List<Point> points = pointDAO.findAll();

		List<Point> sortedPoints = new ArrayList<Point>();

		if (points == null || points.size() == 0)
			return new SortedPoints(0, sortedPoints);

		Iterator<Point> pointIterator = points.iterator();
		Point lastPoint = pointIterator.next();
		pointIterator.remove();

		sortedPoints.add(lastPoint);

		int totalDistance = 0;

		for (int x = 0; x < points.size(); x++) {

			double minDistance = Double.MAX_VALUE;
			Point nearestNeighbour = null;

			for (Point neighbourCandidate : points) {

				if (sortedPoints.contains(neighbourCandidate))
					continue;

				double distance = getDistance(lastPoint, neighbourCandidate);

				if (distance <= minDistance){
					minDistance = distance;
					nearestNeighbour = neighbourCandidate;
				}
			}

			sortedPoints.add(nearestNeighbour);

			lastPoint = nearestNeighbour;

			totalDistance += minDistance;
		}

		totalDistance += getDistance(sortedPoints.iterator().next(), lastPoint);

		logger.debug("Total distance: " + totalDistance);

		return new SortedPoints(totalDistance, sortedPoints);
	}

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_XML)
	public List<Point> getAllPoints() {
		logger.debug("GetAllPoints request...");
		return pointDAO.findAll();
	}

	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Point findById(@PathParam("id") Long id) {	
		if (id != null)
			return pointDAO.findById(id);
		else 
			return null;
	}

	private boolean isValidPoint(Point point){
		return point.getX() != null && point.getY() != null; // TODO other constraints?
	}

	public static boolean isNumeric(String str)
	{
		return str != null && str.matches("-?\\d+(\\.\\d+)?");  
	}

}
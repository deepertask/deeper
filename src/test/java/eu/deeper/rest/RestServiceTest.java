package eu.deeper.rest;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import eu.deeper.rest.model.Point;


public class RestServiceTest {
	
	Logger logger = Logger.getLogger(RestServiceTest.class);
	
	private static final String URI = "http://localhost:8080/Deeper/points/";
	
	private WebResource getWebResource(String path){
		
		Client client = Client.create();

		WebResource webResource = client
		   .resource(URI);
		
		return webResource.path(path);
	}
	
//	@Test
	public void testAdd() {
		
		ClientResponse response = getWebResource("add").type(MediaType.APPLICATION_XML)
		   .post(ClientResponse.class, new Point[]{new Point(1.0,1.0, "red", "red"), new Point(8.0, 8.0), new Point(2.0,2.0,"green", "green"), new Point(4.0, 4.0)});

		Assert.assertNotNull(response); 
		Assert.assertEquals(200, response.getStatus()); 
		
		System.out.println(response.getEntity(String.class));
	}
	
	public static void main(String[] args){
	}

}
